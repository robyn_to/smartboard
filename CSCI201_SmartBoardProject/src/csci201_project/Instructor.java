package csci201_project;

import java.util.ArrayList;

public class Instructor extends User{


	private ArrayList<Course> courses;
	private ArrayList<String> announcements;
	
	public Instructor(int id, String username) {
		super(id, username);
	}

	@Override
	public void openHomePage() {
		// TODO Auto-generated method stub
		
	}
	
	public void make_announcement(String announcement, Course course){
		course.add_announcement(announcement);
		announcements.add(announcement);
	}
	
	public ArrayList<String> get_announcement() {
		return announcements;
	}
	
	public ArrayList<Course> get_all_courses() {
		return courses;
	}
}


