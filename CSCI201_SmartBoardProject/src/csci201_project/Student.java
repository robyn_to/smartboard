package csci201_project;

import java.util.Hashtable;

public class Student extends User {
	private Hashtable<String, Grade> grades; // keytype = String for course name value = ArrayList Grade 
	private Hashtable<String, Course> courses; // keytype = String for course name value = Course 

	public Student(int id, String username) {
		super(id, username);
	}
	public Hashtable<String,Grade> get_all_grades() {
		return grades;
	}

	public Hashtable<String, Course> get_all_courses() {
		return courses;
	}

	public Grade get_single_grade(String course_name) {
		return grades.get(course_name);
	} 

	public Course get_single_course(String course_name) {
		return courses.get(course_name);
	} 

	public void add_course(String course_name, Course course) {
		courses.put(course_name, course);
	}	 

	public void add_grade(String course_name, Grade  grade) {
		grades.put(course_name, grade);
	} 

	public void updatePassword(String newPassword) {
	
	}
	@Override
	public void openHomePage() {
		// TODO Auto-generated method stub
		
	}

}