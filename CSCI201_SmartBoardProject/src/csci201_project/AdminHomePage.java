package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

public class AdminHomePage extends JFrame implements ActionListener {
	
	JPanel jp1;
	JPanel jp2;
	JPanel jp3;
	JPanel jp4;
	JPanel jp5;
	JPanel jspPanel;
	JLabel newsFeed;
	JLabel username;
	JButton sample1;
	JButton sample2;
	JButton sample3;
	JButton sample4;
	JButton sample5;
	JButton logout;
	List<JButton> summary = new ArrayList<JButton>();
	
	JComboBox<String> userBox;
	Vector<String> userClasses = new Vector<String>();
	JScrollPane jsp;
	JRadioButton student; 
	JRadioButton instructor;
	
	private String fontName = "Impact";
	private int fontSize = 48;
	private int fontSize2 = 24;
	
	Color labels;
	Color background;
	Color buttons;
	
	JButton createUser;
	JButton createAnnouncement;
	
	public AdminHomePage() {
		super("SmartBoard");
    	setSize(1000,700);
    	setLocation(150,60);
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    	background = new Color(70,102,117);
    	labels = new Color(148,255,252);
    	buttons = new Color(250,200,191);
    	
    	jp1 = new JPanel();
    	jp1.setBackground(background);

    	student = new JRadioButton("Student");
    	student.setFont(new Font(fontName, Font.BOLD, 24));
    	student.setForeground(labels);
    	instructor = new JRadioButton("Instructor");
    	instructor.setFont(new Font(fontName, Font.BOLD, 24));
    	instructor.setForeground(labels);
    	
    	jp1.setLayout(new FlowLayout());
    	//jp1.add(student);
    	//jp1.add(instructor);
    	
    	createUser = new JButton("Create User");
    	createAnnouncement = new JButton("Create Announcement");
    	
    	jp1.add(createUser);
    	createUser.addActionListener(this);
    	jp1.add(createAnnouncement);
    	createAnnouncement.addActionListener(this);
    	
    	add(jp1);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createUser) {
			SignUpPage sup = new SignUpPage();
			this.setVisible(false);
			sup.setVisible(true);
		}
		
	}
}