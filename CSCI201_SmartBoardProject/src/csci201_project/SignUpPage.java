package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class SignUpPage extends JFrame implements ActionListener{
	
	static SignUpPage sup;
	
	JPanel jp1;
	JPanel jp2;
	JPanel jp3;
	JPanel jp4;
	JPanel jp5;
	JPanel jp6;
	JPanel jp7;
	JPanel jp8;
	JLabel username;
	JLabel password;
	JLabel email;
	JLabel confirmPassword;
	JTextField jtf1;
	JTextField jtf2;
	JTextField jtf3;
	JTextField jtf4;
	JButton done;
	JButton back;
	Color darker;
	Color lighter;
	Color pink;
	JComboBox<String> userBox;
	JRadioButton student;
	JRadioButton instructor;
	Vector<String> userType = new Vector<String>();
	private String fontName = "Impact";
	private int fontSize = 48;
	private int fontSize2 = 24;
	
	public SignUpPage() {
    	super("SmartBoard");
    	setSize(1000,700);
    	setLocation(150,60);
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	darker = new Color(70,102,117);
    	lighter = new Color(148,255,252);
    	pink = new Color(250,200,191);
    	
    	jp1 = new JPanel();
    	jp2 = new JPanel();
    	jp3 = new JPanel();
    	jp4 = new JPanel();
    	jp5 = new JPanel();
    	jp6 = new JPanel();
    	jp7 = new JPanel();
    	jp8 = new JPanel();
    	
    	jp1.setLayout(new BorderLayout(50,50));
    	jp1.setBackground(darker);
    	jp2.setBackground(darker);
    	jp3.setBackground(darker);
    	jp4.setBackground(darker);
    	jp5.setBackground(darker);
    	jp6.setBackground(darker);
    	jp7.setBackground(darker);
    	jp8.setBackground(darker);
    	
    	username = new JLabel("username:");
    	username.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	username.setForeground(lighter);
    	password = new JLabel("password:");
    	password.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	password.setForeground(lighter);
    	email = new JLabel(".edu email:");
    	email.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	email.setForeground(lighter);
    	confirmPassword = new JLabel("confirm password:");
    	confirmPassword.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	confirmPassword.setForeground(lighter);
    	
    	student = new JRadioButton("Student");
    	instructor = new JRadioButton("Instructor");
    	
    	jtf1 = new JTextField(20);
    	jtf2 = new JTextField(20);
    	jtf3 = new JTextField(20);
    	jtf4 = new JTextField(20);
    	jtf1.setMinimumSize(new Dimension(200,40));
    	jtf1.setMaximumSize(new Dimension(200,40));
    	jtf1.setPreferredSize(new Dimension(200,40));
    	jtf2.setMinimumSize(new Dimension(200,40));
    	jtf2.setMaximumSize(new Dimension(200,40));
    	jtf2.setPreferredSize(new Dimension(200,40));    	
    	jtf1.setMinimumSize(new Dimension(200,40));
    	jtf3.setMaximumSize(new Dimension(200,40));
    	jtf3.setPreferredSize(new Dimension(200,40));    	
    	jtf1.setMinimumSize(new Dimension(200,40));
    	jtf3.setMaximumSize(new Dimension(200,40));
    	jtf3.setPreferredSize(new Dimension(200,40));
    	jtf4.setMinimumSize(new Dimension(200,40));
    	jtf4.setMaximumSize(new Dimension(200,40));
    	jtf4.setPreferredSize(new Dimension(200,40));
    	
    	done = new JButton("Create");
    	done.setBackground(pink);
    	done.setOpaque(true);
    	done.setMinimumSize(new Dimension(50,50));
    	done.setMaximumSize(new Dimension(50,50));
    	done.setPreferredSize(new Dimension(50,50));
    	done.addActionListener(this);
    	
    	back = new JButton("<-");
    	jp8.setAlignmentX(Component.LEFT_ALIGNMENT);
    	jp8.add(back);
    	back.addActionListener(this);
    	
    	jp2.setLayout(new FlowLayout(1,20,20));
    	jp2.add(username);
    	jp2.add(jtf1);
    	jp3.setLayout(new FlowLayout(1,20,20));
    	jp3.add(email);
    	jp3.add(jtf2);
    	jp4.setLayout(new FlowLayout(1,20,20));
    	jp4.add(password);
    	jp4.add(jtf3);
    	jp5.setLayout(new FlowLayout(1,20,20));
    	jp5.add(confirmPassword);
    	jp5.add(jtf4);
    	jp6.setLayout(new FlowLayout(1,20,20));
    	jp6.add(student);
    	jp6.add(instructor);
    	
    	jp7.setLayout(new GridLayout(5,1));
    	jp7.setSize(200,200);
    	jp7.add(jp2);
    	jp7.add(jp3);
    	jp7.add(jp4);
    	jp7.add(jp5);
    	jp7.add(jp6);
    	
    	jp1.add(jp8, BorderLayout.NORTH);
    	jp1.add(jp7, BorderLayout.CENTER);
    	jp1.add(done, BorderLayout.SOUTH);
    	add(jp1);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == done) {
			StudentHomePage shp = new StudentHomePage();
			this.setVisible(false);
			shp.setVisible(true);
		}
		else if (e.getSource() == back) {
			AdminHomePage ahp = new AdminHomePage();
			this.setVisible(false);
			ahp.setVisible(true);
		}
		
	}

}
