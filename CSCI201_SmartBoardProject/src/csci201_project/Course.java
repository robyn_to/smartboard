package csci201_project;

import java.util.ArrayList;
import java.util.Hashtable;

public class Course {
	private String course_name;
	private int courseID;
	private Instructor inst;
	private Hashtable<String, Student> students;
	private Hashtable<String, Grade> grades;
	private ArrayList<String> announcements;

	public Course(String course_name, int courseID, Instructor inst){
		this.course_name = course_name;
		this.courseID = courseID;
		this.inst = inst;
	}
	
	public void add_grade(String username, Grade grade) {
		grades.put(username, grade);
	}
	public void remove_student(String username){
		students.remove(username);
	}
	public void add_one_student(String username) {
		// will have to find the student in the list of all of them and then add here
		
	}
	public void add_students(Hashtable students) {
		this.students = students;
	}
	public void add_announcement(String announcement){
		announcements.add(announcement);
	}
	public String get_course_name() {
		return course_name;
	}
}
