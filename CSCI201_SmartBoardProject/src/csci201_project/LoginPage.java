package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginPage extends JFrame implements ActionListener {

	static LoginPage lp;
	
	JPanel jp0;
	JPanel jp1;
	JPanel jp2;
	JPanel jp3;
	JPanel jp4;
	JPanel jp5;
	JPanel jp6;
	JPanel jp7;
	JPanel jp8;
	JLabel name;
	JLabel selectUser;
	JLabel username;
	JLabel password;
	JTextField jtf1;
	JTextField jtf2;
	JButton login;
	JButton signup;
	JButton guest;
	JComboBox<String> userBox;
	Vector<String> userType = new Vector<String>();
	private String fontName = "Impact";
	private int fontSize = 48;
	private int fontSize2 = 24;
	Color labels;
	Color background;
	Color buttons;
	
	String userSelected = "";
	
    public LoginPage() {
    	super("SmartBoard");
    	setSize(1000,700);
    	setLocation(150,60);
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	background = new Color(70,102,117);
    	labels = new Color(148,255,252);
    	buttons = new Color(250,200,191);
    	
    	jp0 = new JPanel();
    	jp1 = new JPanel();
    	jp2 = new JPanel();
    	jp3 = new JPanel();
    	jp4 = new JPanel();
       	jp5 = new JPanel();
       	jp6 = new JPanel();
       	jp7 = new JPanel();
       	jp8 = new JPanel();
       	
    	jp1.setLayout(new BorderLayout(50,50));
    	jp1.setBackground(background);
    	//jp1.setAlignmentX(100);
    	
    	name = new JLabel("SmartBoard");
    	name.setForeground(labels);
    	name.setAlignmentX(Component.CENTER_ALIGNMENT);
    	//name.add(Box.createRigidArea(new Dimension(0,5)));
    	name.setFont(new Font(fontName, Font.BOLD, fontSize));
    	jp0.add(name);
    	jp1.add(jp0, BorderLayout.NORTH);
    	
    	selectUser = new JLabel("What kind of user are you?");
    	selectUser.setFont(new Font(fontName, Font.BOLD, 24));
    	selectUser.setForeground(labels);
    	userBox = new JComboBox<String>(userType);
		userBox.setSelectedItem("Student");
		userType.add("Student");
		userType.add("Instructor");
		userType.add("Administrator");
    	username = new JLabel("username:");
    	username.setFont(new Font(fontName, Font.BOLD, 24));
    	username.setForeground(labels);
    	password = new JLabel("password:");
    	password.setForeground(labels);
    	password.setFont(new Font(fontName, Font.BOLD, 24));
    	

       	//buttonPane.setBackground(background);
    	jtf1 = new JTextField(20);
    	jtf1.setMinimumSize(new Dimension(200,40));
    	jtf1.setMaximumSize(new Dimension(200,40));
    	jtf1.setPreferredSize(new Dimension(200,40));
    	jtf2 = new JTextField(20);
    	jtf2.setMinimumSize(new Dimension(200,40));
    	jtf2.setMaximumSize(new Dimension(200,40));
    	jtf2.setPreferredSize(new Dimension(200,40));
    	
    	jp2.setLayout(new FlowLayout());
    	jp2.add(selectUser);
    	jp2.add(userBox);
    	
    	jp3.setLayout(new FlowLayout(1,20,20));
    	jp3.add(username);
    	jp3.add(jtf1);
    	jp4.setLayout(new FlowLayout(1,20,20));
    	jp4.add(password);
    	jp4.add(jtf2);
    	
    	jp5.setLayout(new GridLayout(3,1));
    	jp5.setSize(200,200);
    	jp5.add(jp2);
    	jp5.add(jp3);
    	jp5.add(jp4);
    	
    	jp0.setBackground(background);
    	jp1.setBackground(background);
    	jp2.setBackground(background);
    	jp3.setBackground(background);
    	jp4.setBackground(background);
    	jp5.setBackground(background);
    	jp6.setBackground(background);
    	jp7.setBackground(background);
    	jp8.setBackground(background);
    	jp1.add(jp5, BorderLayout.CENTER);
    	
    	login = new JButton("Login");
    	login.setBackground(buttons);
    	//login.setForeground(buttons);
    	login.setContentAreaFilled(true);
    	login.setOpaque(true);
    	login.setMinimumSize(new Dimension(150,60));
    	login.setMaximumSize(new Dimension(150,60));
    	login.setPreferredSize(new Dimension(150,60));
    	login.addActionListener(this);
    	signup = new JButton("Sign Up");
    	signup.setBackground(buttons);
    	signup.setOpaque(true);
    	signup.setMinimumSize(new Dimension(150,60));
    	signup.setMaximumSize(new Dimension(150,60));
    	signup.setPreferredSize(new Dimension(150,60));
    	signup.addActionListener(this);
    	guest = new JButton("Guest Trial");
    	guest.setBackground(buttons);
    	guest.setOpaque(true);
    	guest.setMinimumSize(new Dimension(150,60));
    	guest.setMaximumSize(new Dimension(150,60));
    	guest.setPreferredSize(new Dimension(150,60));
    	guest.addActionListener(this);
    	
    	//jp6.setLayout(new FlowLayout(1,100,20));
    	jp6.add(login);
    	//jp6.add(signup);
    	jp7.add(guest);
    	jp8.setAlignmentX(Component.CENTER_ALIGNMENT);
    	jp8.setLayout(new GridLayout(1,2));
    	jp8.add(jp6);
    	jp8.add(jp7);
  
    	jp1.add(jp8, BorderLayout.SOUTH);
    		
    	add(jp1);
    }
	
	public static void main(String [] args) {
    	lp = new LoginPage();
    	lp.setVisible(true);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == login) {
			userSelected = (String)userBox.getSelectedItem();
			System.out.println(jtf1.getText());
			if (userSelected == "Student" && jtf1.getText() != null && jtf2.getText() != null) {
				StudentHomePage shp = new StudentHomePage();
				lp.setVisible(false);
				shp.setVisible(true);
				//THIS SHOULD CONNECT TO THE SERVER
				
			}
			else if (userSelected == "Instructor") {
				InstructorHomePage ihp = new InstructorHomePage();
				lp.setVisible(false);
				ihp.setVisible(true);
			}
			else if (userSelected == "Administrator") {
				AdminHomePage ahp = new AdminHomePage();
				lp.setVisible(false);
				ahp.setVisible(true);
			}
		}
		else if (e.getSource() == guest) {
			GuestTrial gt = new GuestTrial();
			lp.setVisible(false);
			gt.setVisible(true);
		}
		
	}

} 