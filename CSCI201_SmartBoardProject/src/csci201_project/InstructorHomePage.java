package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

public class InstructorHomePage extends JFrame implements ActionListener {

	JPanel jp1;
	JPanel jp2;
	JPanel jp3;
	JPanel jp4;
	JPanel jp5;
	JPanel jp6;
	JPanel jspPanel;
	JLabel courses;
	JLabel username;
	JButton sample1;
	JButton sample2;
	JButton sample3;
	JButton logout;
	JButton selectClass;
	List<JLabel> list = new ArrayList<JLabel>();
	JTable tables;
	
	JComboBox<String> userBox;
	Vector<String> userClasses = new Vector<String>();
	JScrollPane jsp;
	
	private String fontName = "Impact";
	private int fontSize = 48;
	private int fontSize2 = 24;
	
	Color labels;
	Color background;
	Color buttons;
	
	String selectedClass = "";
	
	public InstructorHomePage() {
		super("SmartBoard");
    	setSize(1000,700);
    	setLocation(150,60);
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	background = new Color(70,102,117);
    	labels = new Color(148,255,252);
    	buttons = new Color(250,200,191);
    	
    	jp1 = new JPanel();
    	jp2 = new JPanel();
    	jp3 = new JPanel();
    	jp4 = new JPanel();
    	jp5 = new JPanel();
    	jp6 = new JPanel();
    	jp1.setBackground(background);
    	jp2.setBackground(background);
    	jp3.setBackground(background);
    	jp4.setBackground(background);
    	jp5.setBackground(background);
    	jp6.setBackground(background);
    	
    	jp1.setLayout(new BorderLayout(50,50));
    	
    	courses = new JLabel("Courses");
    	courses.setForeground(labels);
    	courses.setFont(new Font(fontName, Font.BOLD, fontSize));
    	courses.setBackground(labels);
    	courses.setAlignmentX(Component.CENTER_ALIGNMENT);
    	
    	username = new JLabel("Username");
    	username.setForeground(labels);
    	username.setBackground(labels);
    	username.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	username.setAlignmentX(Component.CENTER_ALIGNMENT);
    	
    	logout = new JButton("Logout");
    	logout.addActionListener(this);
    	
    	userBox = new JComboBox<String>(userClasses);
		userBox.setSelectedItem("Student");
		userClasses.add("EE101");
		userClasses.add("CSCI201");
		userClasses.add("CSCI270");
		userClasses.add("MATH225");
		
		sample1 = new JButton("CSCI 270");
		sample2 = new JButton("CSCI 201");
		sample3 = new JButton("EE 101");
	  	sample1.setBackground(buttons);
	  	sample1.setOpaque(true);
	  	sample1.setSize(100, 100);
	  	sample1.setMinimumSize(new Dimension(150,60));
	  	sample1.setMaximumSize(new Dimension(150,60));
	  	sample1.setPreferredSize(new Dimension(150,60));
	  	sample2.setBackground(buttons);
	  	sample2.setOpaque(true);
	  	sample1.setSize(100, 100);
	  	sample2.setMinimumSize(new Dimension(150,60));
	  	sample2.setMaximumSize(new Dimension(150,60));
	  	sample2.setPreferredSize(new Dimension(150,60));
	  	sample3.setBackground(buttons);
	  	sample3.setOpaque(true);
	  	sample3.setMinimumSize(new Dimension(150,60));
	  	sample3.setMaximumSize(new Dimension(150,60));
	  	sample3.setPreferredSize(new Dimension(150,60));
		
		selectClass = new JButton("Go");
		selectClass.addActionListener(this);
		
		jp2.setLayout(new BorderLayout(50,50));
		
		jp6.add(courses);
		
		jp2.add(jp6, BorderLayout.NORTH);
	   	jp3.setLayout(new FlowLayout(1,200,50));
	   	jp3.add(sample1);
	   	jp3.add(sample2);
	   	jp3.add(sample3);
	   	jp3.setAlignmentX(Component.CENTER_ALIGNMENT);
	   	jp2.add(jp3, BorderLayout.CENTER);
	   	
	   	jp4.setLayout(new BorderLayout());
	   	jp5.add(username);
	   	jp5.add(userBox);
	   	jp5.add(selectClass);
	   	jp4.add(jp5, BorderLayout.NORTH);
	   	jp4.add(logout, BorderLayout.SOUTH);
	   	
	   	jp1.add(jp2, BorderLayout.CENTER);
	   	jp1.add(jp4, BorderLayout.EAST);
    	
		add(jp1);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == selectClass) {
			selectedClass = (String)userBox.getSelectedItem();
			if (selectedClass == "EE101") {
				CoursePage cp = new CoursePage();
				this.setVisible(false);
				cp.setVisible(true);
			}
		}
		if (e.getSource() == logout) {
			LoginPage lp = new LoginPage();
			this.setVisible(false);
			lp.setVisible(true);
		}
	}
}
