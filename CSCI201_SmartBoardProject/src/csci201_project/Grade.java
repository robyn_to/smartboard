package csci201_project;

public class Grade {
	int score;
	String grade;
	
	Grade(int score) {
		this.score = score;
		if (score >=93 )
			grade = "A";
		else if (score >=90)
			grade = "A-";
		else if (score >=87)
			grade = "B+";
		else if (score >=83)
			grade = "B";
		else if (score >=80)
			grade = "B-";
		else if (score >=77)
			grade = "C+";
		else if (score >=73)
			grade = "C";
		else if (score >=70)
			grade = "C-";
		else if (score >=60)
			grade = "D";
		else if (score <= 50)
			grade = "F";
	}
	
	public void set_grade(String grade){
		this.grade = grade;
	}
	
	public String get_grade(){
		return grade;
	}
	
	public int get_score() {
		return score;
	}
	
	public void set_score(int score){
		this.score = score;
	}
}
